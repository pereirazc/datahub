CREATE TABLE HIERARQ_ATIVO (
    ORIGEM INT(3),
    IDSITE TEXT(40) NOT NULL,
    SITE TEXT(100),
    IDAREA TEXT(40),
    AREA TEXT(100),
    IDPROCESSO TEXT(40),
    PROCESSO TEXT(100),
    IDLOCINSTALACAO TEXT(40),
    LOCINSTALACAO TEXT(100),
    IDEQUIPAMENTO TEXT(40),
    EQUIPAMENTO TEXT(100),
    DSCEQUIPAMENTO TEXT(400),
    HABILITADO BOOL,
    DATACRIACAO DATETIME,
    DATAULTIMAATUALIZACAO DATETIME,
    LOCALSAPPM DATETIME(48),
    PRIMARY KEY (IDSITE)
);